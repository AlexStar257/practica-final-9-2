package com.example.listview92;

import android.app.Application;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import Modelo.AlumnosDb;

public class Aplicacion extends Application {

    public static ArrayList<Alumno> alumnos;
    private MiAdaptador adaptador;
    private AlumnosDb alumnosDb;

    public ArrayList<Alumno> getAlumnos() {
        return alumnos;
    }
    public MiAdaptador getAdaptador() {
        return adaptador;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        alumnosDb = new AlumnosDb(getApplicationContext());
        alumnos = alumnosDb.allAlumnos();
        alumnosDb.openDataBase();

        Log.d("", "onCreate: Tamaño array list: " + this.alumnos.size());
        this.adaptador = new MiAdaptador(this.alumnos, this);
    }

    public void agregarAlumno(Alumno alumno) {
        alumnosDb.openDataBase();
        long resultado = alumnosDb.insertAlumno(alumno);
        if (resultado != -1) {
            alumno.setId((int) resultado);
            alumnos.add(alumno);
            adaptador.notifyDataSetChanged();
        }
    }
}

