package Modelo;

import com.example.listview92.Alumno;

public interface Persistencia {

     void openDataBase();
     void closeDataBase();
     long insertAlumno(Alumno alumno);
     long updateAlumno(Alumno alumno);
     void deleteAlumno(int id);
}
